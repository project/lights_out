(function ($) {
  $.fn.adjustZindex = function(adjustment) {
    this.each(function(index) {
      // Default to 0 if z-index is NaN
      var zInd = $(this).css('z-index') || 0;
      $(this).css('z-index', parseInt(zInd) + parseInt(adjustment));
    });
    return this;
  };
  $.fn.validateTransparency = function() {
    // Using iFrame tags for Flash objects
    var originalAttr = $(this).attr('src');
    if (originalAttr != undefined) {
      // Check if wmode=transparent parameter is set
      if (originalAttr.indexOf('wmode=transparent') == -1) {
        if (originalAttr.indexOf('?') == -1) {
          $(this).attr('src', originalAttr + '?wmode=transparent');
        }
        else {
          $(this).attr('src', originalAttr + '&wmode=transparent');
        }
      }
    }
    return this;
  };
  Drupal.behaviors.lights_out = {
    toggle_switch: function (clicked_anchor) {
      clicked_anchor = clicked_anchor || $('.lights_out_switch a').first();
      // Get the corresponding anchor label from Drupal
      var switch_anchor_label = Drupal.settings.lights_out_label_off;
      if (clicked_anchor.text() == Drupal.settings.lights_out_label_off) {
        switch_anchor_label = Drupal.settings.lights_out_label_on;
      }
      // Update anchor label and classes
      $('.lights_out_switch a').toggleClass('is-off').toggleClass('is-on').text(switch_anchor_label);
    },
    shadow_show: function (region) {
      // Adjust height of overlay to fill screen when page loads
      $('#lights_out_shadow').css('height', $(document).height());
      // Update z-index of the targeted region
      $(region).adjustZindex(10000);
      $(region).addClass('lights_out_highlight');
      $('#lights_out_shadow').fadeIn();
    },
    shadow_hide: function (region) {
      $('#lights_out_shadow').fadeOut(400, function() {
        // Reset height of overlay to prevent document resizing
        $('#lights_out_shadow').css('height', 0);
      });
      $(region).removeClass('lights_out_highlight');
      // Update z-index of the targeted region
      $(region).adjustZindex(-10000);
    },
    attach: function (context) {
      // Create overlay element
      var newdiv = $('<div id="lights_out_shadow"/>');
      $('body').append(newdiv);
      // Get region classname from Drupal
      var region = '.region-' + Drupal.settings.lights_out_region;
      // Prevent issues with iFrames on regions other than the selected one
      $('*').not(region, region + ' *').find('iframe').each(function(index) {
        $(this).validateTransparency();
      });
      $('.lights_out_switch a').click(function(event) {
        event.preventDefault();
        var clicked_anchor = $(this);
        var state = clicked_anchor.attr('class');
        switch (state) {
          case 'is-on':
            Drupal.behaviors.lights_out.shadow_show(region);
            Drupal.behaviors.lights_out.toggle_switch(clicked_anchor);
            break;

          case 'is-off':
            Drupal.behaviors.lights_out.shadow_hide(region);
            Drupal.behaviors.lights_out.toggle_switch(clicked_anchor);
            break;

          default:
            break;

        }
      });
      $('#lights_out_shadow').click(function() {
        Drupal.behaviors.lights_out.shadow_hide(region);
        Drupal.behaviors.lights_out.toggle_switch();
      });
      return false;
    }
  };
}(jQuery));
