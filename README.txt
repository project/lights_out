CONTENTS OF THIS FILE
---------------------

* Summary
* Installation
* Configuration
* Credits


SUMMARY
-------

A simple module that adds a jquery "lights off" / "lights out" effect to any 
theme region.

Some use cases of this module are:
- Focus user attention to certain areas of the page when an action is required
- Improve the readability of long pieces of text
- Create a "display on foreground" effect when a video is played

The lights out button can be configured as a block or added to
the links area of any content type.


INSTALLATION
------------

See the installation guide here - http://drupal.org/node/70151


CONFIGURATION
-------------

1. Go to settings page: "admin/config/user-interface/lights-out"
2. Select theme region to highlight (exclude from lights_out "shadow" effect)
3. Specify the label/caption for the lights_out links
4. Check all the content types for which this module will be enabled


CREDITS
-------

Current Maintainers:
jcamposanok <http://drupal.org/user/1857856>
sanchi.girotra <http://drupal.org/user/2005932>

If you have any other use cases or ideas for this module,
please feel free to give us the link to the page where it is used
to include it as an example on this module's documentation!
